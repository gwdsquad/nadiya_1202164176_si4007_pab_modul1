package com.example.android.studycase;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText input1;
    EditText input2;
    TextView hasil;
    Button cek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input1 = findViewById(R.id.input1);
        input2 = findViewById(R.id.input2);
        hasil  = findViewById(R.id.hasil);
        cek=findViewById(R.id.cek);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputX1 = input1.getText().toString();
                String inputX2 = input2.getText().toString();

                double inputY1 = Double.parseDouble(inputX1);
                double inputY2 = Double.parseDouble(inputX2);

                double hasilX = (inputY1+inputY2)*2;

                String hasilY = String.valueOf(hasilX);
                hasil.setText(hasilY);
            }
        });
    }

}
